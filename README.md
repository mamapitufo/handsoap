# handsoap

A minimal theme for Hugo, based on [Blank](https://github.com/Vimux/Blank),
created by [Vimux](https://github.com/Vimux).

## Installation

1. Grab the contents of this repository and copy them to the `themes/handsoap`
   directory in your Hugo website.

2. Open `config.toml` in the root directory of your Hugo website and change the
   `theme` to `handsoap`.

   ```toml
   theme = "handsoap"
   ```

## Configuration

You can tweak some site parameters under the `[params]` section on your
`config.toml` file. The defaults for these parameters are:

```toml
[params]
  subtitle = ""             # this will be displayed under the title
  copyright = <site title>  # displayed on the footer copyright notice
  with_goog_font = false    # include Google-hosted Open Sans link?
```

### Menus

The theme has a footer menu, which will be displayed right-aligned, opposite
the copyright notice. To configure it, add `[[menu.footer]]` entries in your
`config.toml` file:

```toml
[menu]
[[menu.footer]]
  name = "code"
  url = "https://gitlab.com/mamapitufo"
[[menu.footer]]
  name = "twitter"
  url = "https://twitter.com/mamapitufo_"
```

----

For more information, read the official Hugo [quick start guide](https://gohugo.io/getting-started/quick-start/).

